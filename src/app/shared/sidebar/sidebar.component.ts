import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  public loading = false
  public navs = [
    // {
    //   name: 'Inbox',
    //   icon: ''
    // },
    // {
    //   name: 'Sent',
    //   icon: ''
    // },
    {
      name: 'Archive',
      icon: ''
    },
    {
      name: 'Trash',
      icon: ''
    }
  ]
  constructor() {}

  ngOnInit(): void {}
}
