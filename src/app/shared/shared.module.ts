import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SidebarComponent } from './sidebar/sidebar.component'
import { HeaderComponent } from './header/header.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { SearchItemPipe } from '../pipes/search-item.pipe'
import { HighlightSearch } from '../pipes/highlight-text.pipe'

@NgModule({
  declarations: [
    SidebarComponent,
    HeaderComponent,
    SearchItemPipe,
    HighlightSearch,
  ],
  imports: [CommonModule, NgbModule],
  exports: [SidebarComponent, HeaderComponent, SearchItemPipe, HighlightSearch],
})
export class SharedModule {}
