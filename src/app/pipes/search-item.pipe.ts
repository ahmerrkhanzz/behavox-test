import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchItem',
})
export class SearchItemPipe implements PipeTransform {
  transform(data: any, term: any): any {
    if (!data) {
      return null;
    }
    if (!term) {
      return data;
    }
    data.map(obj => {
      obj.from = obj.from;
      obj.subject = obj.subject
    });
    return data.filter(item => {
      return JSON.stringify(item)
        .toLowerCase()
        .includes(term.replace(/\s+/g, ' ').toLowerCase());
    });
  }
}
