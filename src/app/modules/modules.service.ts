import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root',
})
export class ModulesService {
  constructor(private http: HttpClient) {}

  getLocationApi(): Observable<any> {
    return this.http.get('https://ipapi.co/json/')
  }
}
