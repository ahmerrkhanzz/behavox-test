export interface Email {
  id: number
  senderName: string
  senderEmail: string
  subject: string
  time: string
  recieverEmail: string
  senderAvatar: string
  description: string
}
