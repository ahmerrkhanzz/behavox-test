import { Component, OnInit, ViewChild } from '@angular/core'
import { HomeService } from './home.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public loading = false
  public data = []
  public searchUser: string = ''
  public pageSize = 10
  public page = 1
  public searchTerm: string
  public selecetdEmail: any = null
  public pages = [5, 10, 20, 30, 50]

  constructor(private homeService: HomeService) {}

  ngOnInit(): void {
    this.getEmails()
  }

  getEmails(): void {
    this.loading = true
    this.homeService.getEmails().subscribe(
      (result) => {
        this.data = result
        this.selecetdEmail = this.data[0]
        this.loading = false
      },
      (err: any) => {
        this.loading = false
      }
    )
  }

  updateSearch(e) {
    this.searchTerm = e.target.value
  }

  emailHandler(item): any {
    this.selecetdEmail = item
  }
}
