import { Component, Input, OnInit } from '@angular/core'

@Component({
  selector: 'app-email-content',
  templateUrl: './email-content.component.html',
  styleUrls: ['./email-content.component.scss'],
})
export class EmailContentComponent implements OnInit {
  public _mail: any
  public _searchTerm: string;
  @Input()
  set searchTerm(data) {
    this._searchTerm = data
  }
  @Input()
  set mail(data) {
    this._mail = data
  }
  constructor() {}

  ngOnInit(): void {}
}
