import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Email } from './email'

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  private emailsUrl = 'api/emails/email.json'

  constructor(private http: HttpClient) {}

  getEmails(): any {
    return this.http.get<Email[]>(this.emailsUrl)
  }
}
