import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { HomeRoutingModule } from './home-routing.module'
import { HomeComponent } from './home.component'
import { FormsModule } from '@angular/forms'
import { SharedModule } from 'src/app/shared/shared.module'
import { HomeService } from './home.service'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { EmailContentComponent } from './email-content/email-content.component';

@NgModule({
  declarations: [HomeComponent, EmailContentComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgbModule,
    CommonModule,
    FormsModule,
    SharedModule,
  ],
  exports: [],
  providers: [HomeService],
})
export class HomeModule {}
